import { Fighter } from './models/fighter';

export async function fight(firstFighter: Fighter, secondFighter: Fighter): Promise<Fighter> {
  // return winner
  const loadingElement: HTMLElement = document.getElementById('fight') as HTMLElement;
  loadingElement.style.visibility = 'visible';

  return await startFight(firstFighter, secondFighter)
  .finally(()=>loadingElement.style.visibility = 'hidden')
  .then(result=>result)
  .catch((error:Error) => {
    throw error;
  });
}

async function startFight(firstFighter: Fighter, secondFighter: Fighter): Promise<Fighter> {
  return new Promise<Fighter>((resolve, reject) => {
    setTimeout(() => {
      try {
        let winner: Fighter = getWinner(firstFighter, secondFighter);
        resolve(winner);
      } catch (error) {
        reject(error);
      }
    }, 500);
  });
}

  function getWinner(firstFighter: Fighter, secondFighter: Fighter): Fighter {
    while (firstFighter.health > 0 || secondFighter.health > 0) {
      let damage: number = getDamage(firstFighter, secondFighter);
      if (secondFighter.health - damage <= 0) {
        secondFighter.health = 0;
        return firstFighter;
      }
      secondFighter.health -= damage;

      damage = getDamage(secondFighter, firstFighter);
      if (firstFighter.health - damage <= 0) {
        firstFighter.health = 0;
        return secondFighter;
      }
      firstFighter.health -= damage;
    }

    throw Error("error occured");
  }

  export function getDamage(attacker: Fighter, enemy: Fighter): number {
    // damage = hit - block
    // return damage 
    const damage: number = getHitPower(attacker) - getBlockPower(enemy);
    return damage > 0 ? damage : 0;
  }

  function generateFighterPower(minValue: number, maxValue: number): number {
    return Math.floor(Math.random() * (maxValue - minValue + 1)) + minValue;
  }

  export function getHitPower(fighter: Fighter): number {
    // return hit power
    const criticalHitChance: number = generateFighterPower(1, 2);
    return fighter.attack * criticalHitChance;
  }

  export function getBlockPower(fighter: Fighter): number {
    // return block power
    const dodgeChance: number = generateFighterPower(1, 2);
    return fighter.defense * dodgeChance;
  }
