interface IFighter {
    readonly _id: string;
    name: string;
    health?: number;
    attack?: number;
    defense?: number;
    source: string;
  }

class Fighter implements IFighter {
    readonly _id: string;
    name: string;
    health: number;
    attack: number;
    defense: number;
    source: string;
  
    constructor(id: string, name: string, health: number, attack: number, defense: number, source: string) {
      this._id = id;
      this.name = name;
      this.health = health;
      this.attack = attack;
      this.defense = defense;
      this.source = source;
    }
  }

  export {IFighter, Fighter};