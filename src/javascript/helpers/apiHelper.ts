import { fightersDetails, fighters } from './mockData';
import {IFighter, Fighter} from '../models/fighter';

const API_URL : string = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI : boolean = true;

interface IApiResponse {
    content: string,
    encoding: string
}

async function callApi(endpoint: string, method: string): Promise<IFighter[] | Fighter>{
  const url: string = API_URL + endpoint;
  const options = {
    method,
  };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
        .then((response) => (response.ok ? <Promise<IApiResponse>>response.json() : Promise.reject(Error('Failed to load'))))
        .then((result) => <IFighter[] | Fighter>JSON.parse(atob(result.content)))
        .catch((error:Error) => {
          throw error;
        });
}

async function fakeCallApi(endpoint: string): Promise<IFighter[] | Fighter> {
  const response: IFighter[] | Fighter = endpoint === 'fighters.json' ? fighters : getFighterById(endpoint);

  return new Promise((resolve, reject) => {
    setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
  });
}

function getFighterById(endpoint: string):Fighter {
  const start:number = endpoint.lastIndexOf('/');
  const end:number = endpoint.lastIndexOf('.json');
  const id:string = endpoint.substring(start + 1, end);

  return <Fighter>fightersDetails.find((it) => it._id === id);
}

export { callApi };
