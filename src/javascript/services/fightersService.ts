import { callApi } from '../helpers/apiHelper';
import {IFighter, Fighter} from '../models/fighter';

export async function getFighters():Promise<IFighter[]> {
  try {
    const endpoint:string = 'fighters.json';
    const apiResult:IFighter[] = <IFighter[]>await callApi(endpoint, 'GET');
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id:string): Promise<Fighter> {
  try {
    const endpoint:string = `details/fighter/${id}.json`;
    const apiResult:Fighter = <Fighter>await callApi(endpoint, 'GET');
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}
