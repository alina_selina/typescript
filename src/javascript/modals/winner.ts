import {Fighter} from '../models/fighter';
import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { attrs } from '../utils/types';

export  function showWinnerModal(fighter:Fighter):void {
  // show winner name and image
  const title:string = 'Winner!';
  const bodyElement: HTMLElement = createWinner(fighter);
  showModal({ title, bodyElement });
}

function createWinner(fighter:Fighter): HTMLElement {
  const { name, source } = fighter;

  const Winner: HTMLElement = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement: HTMLElement = createElement({ tagName: 'span', className: 'fighter-property' });
  const attributes:attrs = { src: source };
  const imgElement:HTMLElement = createElement({ tagName: 'img', className: 'fighter-img', attributes });

  nameElement.innerText = `Name: ${name}`;
  Winner.append(nameElement, imgElement);

  return Winner;
}