import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import {Fighter} from '../models/fighter';
import { attrs } from '../utils/types';

export  function showFighterDetailsModal(fighter:Fighter):void {
  const title:string = 'Fighter info';
  const bodyElement: HTMLElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter:Fighter): HTMLElement {
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails: HTMLElement = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement: HTMLElement = createElement({ tagName: 'span', className: 'fighter-property' });
  const attackElement: HTMLElement = createElement({ tagName: 'span', className: 'fighter-property' });
  const defenseElement: HTMLElement = createElement({ tagName: 'span', className: 'fighter-property' });
  const healthElement: HTMLElement = createElement({ tagName: 'span', className: 'fighter-property' });
  const attributes:attrs = { src: source };
  const imgElement:HTMLElement = createElement({ tagName: 'img', className: 'fighter-img', attributes });
  
  // show fighter name, attack, defense, health, image
  nameElement.innerText = `Name: ${name}`;
  attackElement.innerText = `Attack: ${attack}`;
  defenseElement.innerText = `Defense: ${defense}`;
  healthElement.innerText =`Health: ${health}`;
  fighterDetails.append(nameElement, attackElement,defenseElement,healthElement,imgElement);

  return fighterDetails;
}
