import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { IFighter, Fighter } from './models/fighter';
import { getFighterDetails } from './services/fightersService';
import { handler } from './utils/types';

export function createFighters(fighters: IFighter[]): HTMLElement {
  const selectFighterForBattle: handler = createFightersSelector();
  const fighterElements: HTMLElement[] = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer: HTMLElement = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache: Map<string, Fighter> = new Map();

async function showFighterDetails(event: Event, fighter: IFighter): Promise<void> {
  const fullInfo: Fighter = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: string): Promise<Fighter> {
  // get fighter form fightersDetailsCache or use getFighterDetails function
  if (!fightersDetailsCache.has(fighterId)) {
    const fighter = await getFighterDetails(fighterId);
    fightersDetailsCache.set(fighterId, fighter);
  }

  return <Fighter>fightersDetailsCache.get(fighterId);
}

function createFightersSelector() {
  const selectedFighters: Map<string, Fighter> = new Map();

  return async function selectFighterForBattle(event: Event, fighter: IFighter): Promise<void> {
    const fullInfo: Fighter = await getFighterInfo(fighter._id);

    if ((<HTMLInputElement>event.target).checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else {
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      const iterator: IterableIterator<Fighter> = selectedFighters.values();
      const winner: Fighter = await fight(<Fighter>iterator.next().value, <Fighter>iterator.next().value);
      showWinnerModal(winner);

      for (let fighter of document.getElementsByClassName('fighter'))
        fighter.getElementsByTagName("input")[0].checked = false;

      selectedFighters.clear();
    }
  }
}
