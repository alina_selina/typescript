import {IFighter} from '../models/fighter';

export type handler = (event:Event, fighter:IFighter)=>Promise<void>;
export type eventHandler = (event:Event)=>void;
export type attrs = {[key:string]:string};