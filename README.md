# Hometask Typescript

## Доделать проект и добавить типизацию

В файле fighterService создать функцию для получения детальной информации для бойца. Добавить возможность ее просмотра. Для этого реализовать функцию getFighterInfo, которая бы обработала клик по бойцу. Данные отобразить с помощью функции showFighterDetailsModal.

Процесс битвы будет запускаться автоматически, когда бойцы будут выбраны с помощью соответствующих чекбоксов. Создать функцию fight, которая принимает как параметры, так и запускает процесс игры. Игроки наносят удары друг другу по очереди, а уровень их health уменьшается на getDamage. Если один из них умирает, то игра заканчивается и функция должна вернуть победителя. Имя победителя выводится на экран с помощью функции showWinnerModal.

Реализовать функции:

* getDamage, расчет урона здоровья соперника по формуле hitPower - blockPower,
* getHitPower, расчет силы удара (размер урона здоровью соперника) по формуле power = attack * criticalHitChance;, где criticalHitChance — случайное число от 1 до 2,
* getBlockPower, расчет силы блока (амортизация удара соперника) по формуле power = defense * dodgeChance;, де dodgeChance — случайное число от 1 до 2.

## Instalation

`npm install`

`npm start`

open http://localhost:8080/

## UPD ##

Dockerhub [link](https://hub.docker.com/repository/docker/alinablankselina/street-fighter)
